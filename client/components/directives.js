angular.module('myApp') 

// NAVIGATION DIRECTIVES

	.directive('mainNav' , function() {
	return {
	
			restrict: 'E',
			templateUrl: '../components/nav/nav.tmpl.html'
		}
	})
	.directive('horNav', function(mapService) {
		return {
	
			restrict: 'E',
			templateUrl: '../components/nav/hornav.tmpl.html',
			controller: 'listController',
			controllerAs: 'listC'


		}
	})
	.directive('tickerDiv', function () {

		return {

			restrict: 'E',
			templateUrl: '../components/ticker/ticker.tmpl.html',
			controller: 'tickerCtrl',
			controllerAs: "ticker"
		}
	})

// NAVIGATION DIRECTIVES END

// HEADERS DIRECTIVES

	.directive('eventHead', function () {
		return {
	
			restrict: 'E',
			templateUrl: '../components/header/eventhead.tmpl.html'	
		}
});

// HEADERS DIRECTIVES END

