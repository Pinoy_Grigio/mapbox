angular.module('myApp')

.service('tickerService', function () {


	return {

		// things: function(){
		// 	console.log('service is linking!');
		// }

		hashtags: {

			messsage: '#lifeontherow',
			id: 'saved mongo id',
			date: 'dates saved from mongo, Not date of event',
			time: 'time of posted tweet'
		},

		events: {

			title: 'event title',
			messsage: 'event messsage',
			link: 'picture link to saved picture',
			start: 'event start',
			end: 'event end',
			id: 'mongo saved id, for refrence and feedback in CMS',
			date: 'saved date from mongo, Not date of event'
		}

	};


});