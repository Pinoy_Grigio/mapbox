angular.module('myApp')

.controller('listController', ['mapService', '$http', function(mapService, $http) {
	var listC = this;

	listC.prom = mapService.getTheFiles
		.success(function(res) {
			listC.establishments = res;
		})
		.error(function(err) {
			console.log(err);
		});

	// seems redundant w/ listC.flyTo
	listC.selectedStore = mapService.selectedStore;

	listC.lastSelected = mapService.lastSelected;
    listC.tabClick = function(s) {
    	mapService.tabClick(s);
    	listC.lastSelected = mapService.lastSelected;
    }

	listC.flyTo = function(destination) {
		mapService.flyTo(destination);
		listC.selectedStore = mapService.selectedStore;
	};

	listC.directions = function(a) {
		mapService.directions(a);
	}
}]);
