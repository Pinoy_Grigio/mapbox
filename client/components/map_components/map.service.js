angular.module('myApp')
.service('mapService', ['$q', '$http', function($q, $http) {
	var mapService = this;
	// mapService.map;
	// var map = mapService.map;
	mapService.getTheFiles = $http.get('../assets/buildings.json');


	mapService.establishments = {};
	

	mapService.initialize = function() {
		var deferred = $q.defer();
		mapboxgl.accessToken = 'pk.eyJ1IjoiYmVybWFuYXRvciIsImEiOiJjaXE5eDZocGUwMW1jZnRtMTJqbWRtOWE3In0.2Fe9O3nEayrmWuSFUcM27w';
		var bounds = [
		    [-121.98, 37.31], // (southwest)
		    [-121.93, 37.33] // (northeast)
		]; 

		mapService.map = new mapboxgl.Map({
		    container: 'map',
		    // style: 'mapbox://styles/bermanator/ciqpta9yf0043bhnfasufn8dn',
		    // style: 'mapbox://styles/bermanator/ciqskb51w0001jsm5ll8iy18f',
		    // style: 'mapbox://styles/bermanator/cirry4qju003mg9nggzc62yvn', 
		    style: 'mapbox://styles/bermanator/ciqsjhpbh0007ckmclaa8hrek',
		    pitch: 80,
		    center: [-121.9479364, 37.32080813], // starting position
		    zoom: 19.5,
		    maxZoom: 26,
		    minZoom: 10,
		    bearing: 0,
		    maxBounds: bounds
		});
		// nice red color for street... hsl(359, 47%, 33%)

	    mapService.map.on('click', function(e) {
	        var cords = JSON.stringify(e.lngLat);
	        console.log(cords);
	    });

	    mapService.directionTime = new mapboxgl.Directions({
			proximity: [-121.94824, 37.32057],
			profile: 'walking',
			unit: 'imperial',
			controls: {
				inputs: true,
				instructions: true
			}
	    });

		mapService.map.addControl(mapService.directionTime);
		mapService.map.on('load', function() {
			mapService.directionTime.setOrigin([-121.9479364, 37.32048513]);
			// mapService.loadLayers();
			mapService.getTheFiles.then(function(res) {
		mapService.establishments = res.data;
			deferred.resolve('yes it works');
	});
		});
		return deferred.promise;
	};


	mapService.loadLayers = function() {
	    	 // adding layers:
		// mapService.map.on('load', function () {
			console.log(mapService.establishments)
		    mapService.map.addSource("home", {
		        "type": "geojson",
		        "data": mapService.establishments.home
		    });
		    mapService.map.addLayer({
		        "id": "home",
		        "type": "symbol",
		        "source": "home",
		        "layout": {
		        	"visibility": "visible",
		            "icon-image": "clothing-store-15",
		            "icon-size": 1.5,
		            // "text-field": "{title}",
		            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		            "text-offset": [0, 0.6],
		            "text-anchor": "top"
		        }
		    });

		    mapService.map.addSource("restaurants", {
		        "type": "geojson",
		        "data": mapService.establishments.restaurants
		    });
		    mapService.map.addLayer({
		        "id": "restaurants",
		        "type": "symbol",
		        "source": "restaurants",
		        "layout": {
		        	"visibility": "visible",
		            "icon-image": "restaurant-15",
		            "icon-size": 1.5,
		            // "text-field": "{title}",
		            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		            "text-offset": [0, 0.6],
		            "text-anchor": "top"
		        }
		    });

		    mapService.map.addSource("salon", {
		        "type": "geojson",
		        "data": mapService.establishments.salon
		    });
		    mapService.map.addLayer({
		        "id": "salon",
		        "type": "symbol",
		        "source": "salon",
		        "layout": {
		        	"visibility": "none",
		            "icon-image": "bar-15",
		            "icon-size": 1.5,
		            // "text-field": "{title}",
		            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		            "text-offset": [0, 0.6],
		            "text-anchor": "top"
		        }
		    });

		    mapService.map.addSource("specialty", {
		        "type": "geojson",
		        "data": mapService.establishments.specialty
		    });
		    mapService.map.addLayer({
		        "id": "specialty",
		        "type": "symbol",
		        "source": "specialty",
		        "layout": {
		        	"visibility": "none",
		            "icon-image": "doctor-15",
		            "icon-size": 1.5,
		            // "text-field": "{title}",
		            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		            "text-offset": [0, 0.6],
		            "text-anchor": "top"
		        }
		    });

		    mapService.map.addSource("apparel", {
		        "type": "geojson",
		        "data": mapService.establishments.apparel
		    });
		    mapService.map.addLayer({
		        "id": "apparel",
		        "type": "symbol",
		        "source": "apparel",
		        "layout": {
		        	"visibility": "none",
		            "icon-image": "bank-15",
		            "icon-size": 1.5,
		            // "text-field": "{title}",
		            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		            "text-offset": [0, 0.6],
		            "text-anchor": "top"
		        }
		    });
			    // adding accessories
		    mapService.map.addSource("accessories", {
		        "type": "geojson",
		        "data":mapService.establishments.accessories
		    });
		    mapService.map.addLayer({
		        "id": "accessories",
		        "type": "symbol",
		        "source": "accessories",
		        "layout": {
		        	"visibility": "none",
		            "icon-image": "bus-15",
		            "icon-size": 1.5,
		            // "text-field": "{title}",
		            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
		            "text-offset": [0, 0.6],
		            "text-anchor": "top"
		        }
		    });
		// });
	};



	mapService.lastSelected = 'restaurants';


    mapService.tabClick = function(layer) {
    	// mapService.map.flyTo({})
    	// ^^^idea for later^^

    	var map = mapService.map;
	    if (layer == undefined) {
	    	layer = 'restaurants';
	    };

	    console.log(layer);
	    var id = layer;

	    console.log(map.getLayer(id));


	    var visibility = map.getLayoutProperty(id, 'visibility');
	    console.log(visibility);
    	mapService.map.setLayoutProperty(id, 'visibility', 'visible');
    	if (mapService.lastSelected !== id) {
	    	mapService.map.setLayoutProperty(mapService.lastSelected, 'visibility', 'none');
    	mapService.lastSelected = layer;

	    };
	};



	mapService.flyTo = function(destination) {
		mapService.clearDirections();
		mapService.map.flyTo({
			speed: 5,
			curve: 1,
			bearing: destination.geometry.bearing,
			pitch: destination.geometry.pitch,
			duration: 3500,
			zoom: 19.5,
	        center: [destination.geometry.coordinates[0],
	            destination.geometry.coordinates[1]],
	    });
		mapService.addPopup(destination);
		var parent = mapService.popDiv.parentElement.parentElement;
		parent.classList.add('popup');
		mapService.addBounce();
		mapService.selectedStore = destination;
		console.log(mapService.selectedStore);
	};

	mapService.youAreHere = function() {
		mapService.popDiv = document.createElement('div');
		mapService.popDiv.innerHTML = 'You Are Here';
		mapService.popup = new mapboxgl.Popup({closeButton: false})
			.setLngLat([-121.9479364, 37.32080813])
			.setDOMContent(mapService.popDiv)
			.addTo(mapService.map);
			console.log(mapService.popDiv);
		var parent = mapService.popDiv.parentElement.parentElement;
		parent.classList.add('popup', 'popupAnimate');
	};

	mapService.addPopup = function(a) {
		mapService.popup.remove();
		mapService.popDiv.innerHTML = a.properties.name;
		console.log(a.geometry.coordinates[0]);
		mapService.popup = new mapboxgl.Popup({closeButton: false})
			.setLngLat([a.geometry.coordinates[0], a.geometry.coordinates[1]])
			.setDOMContent(mapService.popDiv)
			.addTo(mapService.map);
	};

	// re-do this
	mapService.addBounce = function() {
		var parent = mapService.popDiv.parentElement.parentElement;
		window.setTimeout(function() {
			parent.classList.add('popupAnimate2');
		}, 2000);
	};

	mapService.directions = function(a) {
		directions_list.style.display = 'block';
		var coordi1 = a.geometry.coordinates[0],
			coordi2 = a.geometry.coordinates[1];
		mapService.addPopup(a);
		var parent = mapService.popDiv.parentElement.parentElement;
		parent.classList.add('popup');
		mapService.directionTime.setOrigin([-121.9479364, 37.32048513]);
		mapService.directionTime.setDestination([coordi1, coordi2]);
		mapService.selectedStore = a;
	};

	mapService.selectedStore = {
		properties: {
			address: "Select Your DESTINATION",
		}
	};

	mapService.clearDirections = function() {
		var directions_list = document.getElementById('directions_list');
		var ex_out = document.getElementsByClassName('geocoder-icon-close')[1];
		ex_out.click();
		console.log(ex_out);
	};
}]);
