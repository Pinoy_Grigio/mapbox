angular.module('myApp').directive('myDirective', ['mapService', function(mapService) {
	return {
		restrict: 'E',
		templateUrl: 'components/map_components/map.tmpl.html',
		controller: 'listController',
		controllerAs: 'listC',
		link: function(scope, element, attrs) {
			// edwin
			var promise = mapService.initialize();
			promise.then(function(dat) {
				console.log(dat);
				mapService.youAreHere();
				mapService.loadLayers();
				// mapService.tabClick();
			}, function(error){
				console.log(error)
			});
		}
	};
}]);