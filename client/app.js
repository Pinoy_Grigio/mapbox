var app = angular.module('myApp', ['ui.router']);


app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider

	.state('map', {
		url : '/map',
		templateUrl: 'view1/map.html'
	})
	.state('events', {
		url: '/events',
		templateUrl: 'view2/events.html'
	})
	.state('photo', {
		url: '/photo',
		templateUrl: 'view3/photo.html'
	});
	$urlRouterProvider.when('','/map');
});

// app.directive('navRoutes', function () {
// 	return {

// 		restrict: 'E',
// 		templateUrl: 'templates/nav.html'
// 	}

// });


